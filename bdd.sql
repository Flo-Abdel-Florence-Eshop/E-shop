-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: db
-- ------------------------------------------------------
-- Server version	5.5.5-10.3.9-MariaDB-1:10.3.9+maria~bionic

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Dessert au chocolat'),(2,'Barre Chocolatée'),(6,'Idée cadeau');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20180907135740'),('20180907141935');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D34A04AD12469DE2` (`category_id`),
  CONSTRAINT `FK_D34A04AD12469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,1,'Brownie','Le brownie est un gâteau au chocolat, fondant par endroits, cuit au four. Un glaçage peut être ensuite déposé sur sa surface. Sa crème de préparation peut également être mangée sans être cuite.',20,'475ce8bc466ef68611d04d373a2c4ffc.jpeg'),(2,1,'Craquant au chocolat','Pour le croquant, faire fondre la pralinoise, y ajouter les crêpes émiettées et 40 g de pralin, bien mélanger, étaler sur un plat de service et faire durcir 30 minutes au frigo.',30,'85030bf3038d6557008ec2c22bde86c2.jpeg'),(3,1,'Fondant au Chocolat','Le mi-cuit au chocolat ou gâteau au cœur coulant est un type de gâteau au chocolat. Le mi-cuit est réalisé grâce à une cuisson rapide qui assure un cœur coulant et un extérieur cuit. Michel Bras est le créateur d\'une recette de mi-cuit au chocolat, le « biscuit de chocolat coulant ».',25,'b869727d7cef2b2386bf47772382f1d5.jpeg'),(4,1,'Délice au Chocolat','Casser le chocolat en morceaux et le faire fondre avec le beurre au bain-marie ou au micro-ondes allure faible. Ajouter le sucre en poudre et, en remuant avec une cuillère en bois, les 3 jaunes d\'oeufs un à un, puis la farine.',35,'3324dddcb0c24d7d822d52522415f241.jpeg'),(5,2,'Chocolat','Chocolat raffiné à 90%',15,'5466d599f821622f1e00495736b1c68d.png'),(11,2,'Chocolat','Chocolat raffiné à 52 %',15,'8af5100c06be7ba2935ee4fb060358dd.png'),(12,2,'Chocolat','Chocolat rafiné à 70 %',15,'3bb69f0df564307738ee8e1245210578.png'),(13,2,'Chocolat','Chocolat raffiné à 60 %',15,'f309e40945cf683dd859258d384a4e5e.png'),(16,6,'Billes de chocolat','Chocolat à partager pour le plaisir',15,'6bc081713f9d77c8e357b927049585ab.png'),(17,6,'Billes de chocolat','Chocolat à partager pour le plaisir',15,'5582348568f298f0efb8fb4fc3779352.png'),(18,6,'Billes de chocolat','Chocolat à partager pour le plaisir',15,'c4490d7d5fee4fdf39e4a337a9e596fe.png'),(26,1,'Gourmandises au Chocolat','Délicieux petits gâteaux au chocolat',20,'06f3e007d9313594b33eae68c1c19e7b.jpeg'),(27,1,'Cake au Chocolat','Cake fondant au chocolat pour un dessert gourmand',40,'ee6989226739cb207db19a009edb5860.jpeg'),(28,1,'Assortiment de Chocolats','Assortiment de petits chocolats gourmand',30,'d4988e68fdb53b95317b3bbe5a384e72.jpeg'),(29,1,'Petits Chocolats','Chocolats gourmands à la cannelle pour les petites faim',10,'9e647339348fd4088b01a3f79dc4c0dd.jpeg');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_line`
--

DROP TABLE IF EXISTS `product_line`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_line` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shopping_cart_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `price` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5CFC965745F80CD` (`shopping_cart_id`),
  KEY `IDX_5CFC96574584665A` (`product_id`),
  CONSTRAINT `FK_5CFC96574584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_5CFC965745F80CD` FOREIGN KEY (`shopping_cart_id`) REFERENCES `shopping_cart` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_line`
--

LOCK TABLES `product_line` WRITE;
/*!40000 ALTER TABLE `product_line` DISABLE KEYS */;
INSERT INTO `product_line` VALUES (1,3,1,2,40),(2,3,2,1,30),(3,3,3,1,25),(6,3,4,1,35),(36,7,1,2,40),(38,3,11,1,15);
/*!40000 ALTER TABLE `product_line` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shopping_cart`
--

DROP TABLE IF EXISTS `shopping_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shopping_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_72AAD4F6A76ED395` (`user_id`),
  CONSTRAINT `FK_72AAD4F6A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shopping_cart`
--

LOCK TABLES `shopping_cart` WRITE;
/*!40000 ALTER TABLE `shopping_cart` DISABLE KEYS */;
INSERT INTO `shopping_cart` VALUES (1,NULL),(2,NULL),(4,NULL),(5,NULL),(6,NULL),(3,1),(7,2);
/*!40000 ALTER TABLE `shopping_cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'ligonnet','Florian','florian.ligonnet@gmail.com','1989-07-02','$2y$12$WLhXvvOW22hytH4pQmxO..ajmrnOVtd0DzHOO/Ps.XTPWljYM1GR6',1),(2,'test','testuser','testuser@test.fr','1926-01-01','$2y$12$p9V2OAWvRQcLXD1LwbqU6.MRMs/DJSERPETsiBP50Mt5gO6cju9o2',0),(3,'testadmin','testAdmin','testadmin@test.fr','1906-08-01','$2y$12$xy/LCWwvkJj9.1lNSK9KOOaMyXImd4ywP0vV4TSNM9u.EcRL1Gavi',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-21 13:39:36
