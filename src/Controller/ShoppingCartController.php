<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\ShoppingCart;

class ShoppingCartController extends Controller
{
    /**
     * @Route("user/shopping/cart", name="shopping_cart")
     */
    public function index(Session $session = null, ObjectManager $manager)
    {
        

            $cart =$this->getUser()->getShoppingCart();
            if(!$cart){
                $cart = new ShoppingCart();
            }

            $totalPrice = 0;
            foreach($cart->getProductLine() as $line) {
                $totalPrice = $totalPrice + $line->getPrice();
            }
            
        

            $cart = $this->getDoctrine()->getManager()->merge($cart);
            $productLine = $cart->getProductLine();

            $manager->persist($cart);
            $manager->flush();
            
        return $this->render('shopping_cart/index.html.twig', [
            "productLine" => $productLine,
            "totalPrice" => $totalPrice
        ]);
    }

}
