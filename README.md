# E-shop

Il s'agit d'un projet de groupe réalisé dans le cadre d'une formation.

fait part: 
Florian LIGONNET
Florence COLONNELLO
Abdelrazak SAKHRI

Dans un premier temps se connecter à un des comptes existants ci dessous ou créer un compte afin d'accéder aux fonctionnalités du site.

Pour tester l'interface Administrateur ( ajout, mise à jour de produits ) ;
identifiant : testadmin@test.fr
password : azerty


Pour tester l'interface Utilisateur ( ajout de produits au panier ) ;
identifiant : testuser@test.fr
password : azerty
